# Rescramble base
if [[ "$POLYVERSE_POLYMORPHIC_LINUX_KEY" != "" ]]; then \
  echo "Rescrambling all packages on this image..." && \
  echo "Using Scrambling key: $POLYVERSE_POLYMORPHIC_LINUX_KEY" && \
  apk update && \
  apk upgrade && \
  apk add curl && \
  curl https://repo.polyverse.io/install.sh | sh -s "$POLYVERSE_POLYMORPHIC_LINUX_KEY" && \
  apk update && \
  apk upgrade && \
  sed -n -i '/repo.polyverse.io/p' /etc/apk/repositories && \
  apk upgrade --update-cache --available && \
  echo "Scrambling complete. Number of packages scrambled: " && \
  apk info | xargs apk policy | grep -i polyverse | wc -l && \
  echo "Total packages on this system:" && \
  apk info | wc -l; \
fi
